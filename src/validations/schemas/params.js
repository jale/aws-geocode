'use strict'

const Joi = require('joi')

module.exports = Joi.object().keys({
      text: Joi.string().required(),
      outSR: Joi.string().alphanum().required(),
	  f: Joi.string().alphanum().required(),
	  regionCodes: Joi.string().alphanum().required(),
	  outFields: Joi.string().required(),
})