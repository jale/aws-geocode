'use strict'

const schema = require('./validations/schemas/params')
const { Client } = require('pg')

module.exports.geocode = async event => {
  
  const queryString = event.queryStringParameters
  const { error, value } = schema.validate(queryString)
  
  if(error){
    return {statusCode: 400, body: JSON.stringify({message: `Input data not valid.`},null,2)}
  }else{
    const tsQuery = queryString.text.split(" ").filter((n) => {return n != ""}).map(x => x + ':*').join(' & ');
    const client = new Client()
    await client.connect()
    const res =  await client.query(`SELECT 
                                        'adr' as type,
                                        'adresa' as typeDescCz,
                                        'address' as typeDescEn,
                                        'address' as layer_name,
                                        label,
                                        kod as id,
                                        st_x(st_transform(geom, ${queryString.outSR})) as x,
                                        st_y(st_transform(geom,${queryString.outSR})) as y,
                                        ts_rank_cd(data, to_tsquery('simple','${tsQuery}')) AS rank  
                                     FROM ruian.adresy WHERE data @@ to_tsquery('simple','${tsQuery}') order by rank desc limit 20;`)
    await client.end() 
    var geojson = '{"type":"FeatureCollection","features":['
    for (let row of res.rows) {
        geojson += `{"type":"Feature","geometry":{"type":"Point","coordinates":[${row.x},${row.y}]},"properties":{"type":"adr","label":"${row.label}","typeDescCz":"adresa","typeDescEn":"address","layer_name":"address","id":${row.id},"score":${row.rank}}},`
    }
    if (res.rows.length > 0) geojson = geojson.slice(0, -1)
    geojson += `],"totalFeatures":${res.rows.length}}`
    
    return {
      statusCode: 200,
      //body: JSON.stringify(tsQuery)
     body: geojson
    }
  }
}